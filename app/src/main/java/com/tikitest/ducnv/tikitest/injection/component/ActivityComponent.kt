package com.tikitest.ducnv.tikitest.injection.component

import android.app.Application
import com.tikitest.ducnv.tikitest.injection.module.Activitymodule
import com.tikitest.ducnv.tikitest.injection.module.ApplicationModule
import com.tikitest.ducnv.tikitest.injection.module.NetworkModule
import com.tikitest.ducnv.tikitest.MyApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Created by duc on 5/1/2018.
 */
@Singleton
@Component(modules = [NetworkModule::class, Activitymodule::class, ApplicationModule::class, AndroidSupportInjectionModule::class])
interface ActivityComponent : AndroidInjector<MyApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ActivityComponent
    }
}