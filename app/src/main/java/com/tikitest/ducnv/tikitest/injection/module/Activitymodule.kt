package com.tikitest.ducnv.tikitest.injection.module
import com.tikitest.ducnv.tikitest.injection.component.ActivityScoped
import com.tikitest.ducnv.tikitest.ui.view.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by duc on 5/3/2018.
 */

@Module
abstract class Activitymodule {
    @ActivityScoped
    @ContributesAndroidInjector(modules = [(MainPresenterModule::class)])
    abstract fun mainActivity(): MainActivity

}