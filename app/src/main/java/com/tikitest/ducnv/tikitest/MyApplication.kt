package com.tikitest.ducnv.tikitest

import com.tikitest.ducnv.tikitest.injection.component.DaggerActivityComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class MyApplication : DaggerApplication() {
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerActivityComponent.builder().application(this).build()
    }
}