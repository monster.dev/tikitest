package com.tikitest.ducnv.tikitest.utils

import android.graphics.Color
import android.text.TextUtils
import android.util.Log
import java.util.*

object Constants {
    val BASEURL = "https://raw.githubusercontent.com"

    fun getKeyword(original: String ?): String {
        if(original.isNullOrEmpty()) return ""
        var output = ""
        val length = original.length / 2

        val s1 = original.substring(0, length)
        val s2 = original.substring(length, original.length)

        if (s1.endsWith(" ") || s2.startsWith(" ")) {
            output = s1 + "\n" + s2
            Log.e("original", "1----->$output")
            return output.trim()
        }

        val arrLeft = s1.split(" ").dropLastWhile { it.isEmpty() }.toTypedArray()
        val arrRight = s2.split(" ").dropLastWhile { it.isEmpty() }.toTypedArray()

        val listLeft = ArrayList(Arrays.asList<String>(*arrLeft))
        val listRight = ArrayList(Arrays.asList<String>(*arrRight))

        output = if (listLeft[listLeft.size - 1].length > listRight[0].length) {
            val temp = listRight.removeAt(0)
            TextUtils.join(" ", listLeft) + temp + "\n" + TextUtils.join(" ", listRight)
        } else {
            val temp = listLeft.removeAt(listLeft.size - 1)
            TextUtils.join(" ", listLeft) + "\n" + temp + TextUtils.join(" ", listRight)
        }

        return output.trim()
    }

    fun color(): Int {
        val rnd = Random()
        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))
    }
}