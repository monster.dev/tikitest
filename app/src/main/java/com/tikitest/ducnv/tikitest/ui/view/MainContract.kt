package com.tikitest.ducnv.tikitest.ui.view

import com.tikitest.ducnv.tikitest.base.BaseView
import com.tikitest.ducnv.tikitest.base.Basepresenter


interface MainContract {
    interface View : BaseView<MainPresenter> {
        fun showData(listKey: List<String>)
        fun showError(message : String)
    }

    interface MainPresenter : Basepresenter<View> {
        fun getKeyword()

        override fun attachView(view: View)

        override fun detachView()

    }
}