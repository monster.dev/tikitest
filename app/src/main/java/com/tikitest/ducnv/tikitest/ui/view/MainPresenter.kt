package com.tikitest.ducnv.tikitest.ui.view

import android.annotation.SuppressLint
import android.util.Log
import com.tikitest.ducnv.tikitest.network.NetworkService
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MainPresenter @Inject constructor(var networkService: NetworkService) : MainContract.MainPresenter {
    var mView: MainContract.View? = null
    var mDisposable: Disposable? = null

    @SuppressLint("CheckResult")
    override fun getKeyword() {
        networkService.getKeyword()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribeWith(object : Observer<List<String>> {
                override fun onComplete() {
                    Log.e("MainPresenter", "onComplete")
                }

                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(t: List<String>) {
                    Log.e("MainPresenter", "onNext")
                        mView?.showData(t)

                }

                override fun onError(e: Throwable) {
                    Log.e("MainPresenter", "onError" + e.message)
                    e.message?.apply {
                        mView?.showError(this)
                    }


                }
            })

    }

    override fun attachView(view: MainContract.View) {
        mView = view
    }

    override fun detachView() {
        mView = null
        if (mDisposable != null) mDisposable!!.dispose()
    }
}