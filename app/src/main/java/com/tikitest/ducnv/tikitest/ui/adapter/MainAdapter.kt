package com.tikitest.ducnv.tikitest.ui.adapter

import android.graphics.Color
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.tikitest.ducnv.tikitest.R
import com.tikitest.ducnv.tikitest.utils.Constants
import java.util.*

class MainAdapter(var keywordList: List<String>) : RecyclerView.Adapter<MainAdapter.ViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val inflater = LayoutInflater.from(p0.context)
        val view = inflater.inflate(R.layout.item_key_word, p0, false)
        val height = p0.context.resources.getDimension(R.dimen.padding_8) * 2 +
                p0.context.resources.getDimension(R.dimen.padding_16) * 2 +
                p0.context.resources.getDimension(R.dimen.padding_16) * 0.5
        view.layoutParams.height = height.toInt()
        return MainAdapter.ViewHolder(view)
    }

    override fun getItemCount(): Int = keywordList.size

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.tvKeyword.setBackgroundColor(Constants.color())
        p0.tvKeyword.text = keywordList[p1]

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvKeyword = view.findViewById<AppCompatTextView>(R.id.tv_key_word)!!
    }

    fun addData(list: List<String>) {
        keywordList = list
    }
}