package com.tikitest.ducnv.tikitest.network

import io.reactivex.Observable
import retrofit2.http.GET

/**
 * Created by duc on 5/1/2018.
 */

interface NetworkService {
    @GET("tikivn/android-home-test/v2/keywords.json?")
    fun getKeyword(): Observable<List<String>>
}