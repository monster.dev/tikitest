package com.tikitest.ducnv.tikitest.injection.module

import com.tikitest.ducnv.tikitest.injection.component.ActivityScoped
import com.tikitest.ducnv.tikitest.ui.view.MainContract
import com.tikitest.ducnv.tikitest.ui.view.MainPresenter
import dagger.Binds
import dagger.Module

@Module
abstract class MainPresenterModule{
    @ActivityScoped
    @Binds
    abstract fun musicPlayerPresenter(presenter: MainPresenter) : MainContract.MainPresenter
}