package com.tikitest.ducnv.tikitest.ui.view

import android.os.Bundle
import android.widget.Toast
import com.tikitest.ducnv.tikitest.R
import com.tikitest.ducnv.tikitest.ui.adapter.MainAdapter
import com.tikitest.ducnv.tikitest.utils.Constants
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity(), MainContract.View {

    @Inject
    lateinit var mPresenter: MainPresenter
    lateinit var mMainAdapter: MainAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mPresenter.attachView(this)
        initRecyclerView()
        mPresenter.getKeyword()
    }

    override fun showData(listKey: List<String>) {
        mMainAdapter.addData(handleKeyword(listKey))
        mMainAdapter.notifyDataSetChanged()
    }

    override fun showError(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun onDestroy() {
        mPresenter.detachView()
        super.onDestroy()
    }

    fun initRecyclerView() {
        var data: ArrayList<String> = ArrayList()
        mMainAdapter = MainAdapter(data)
        recycler_key_word.adapter = mMainAdapter
    }

    fun handleKeyword(listKey: List<String>): ArrayList<String> {
        var output: ArrayList<String> = ArrayList()
        for (i in listKey.indices) {
            output.add(Constants.getKeyword(listKey[i]))
        }
        return output
    }

}
