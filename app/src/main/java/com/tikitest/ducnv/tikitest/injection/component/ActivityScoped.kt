package com.tikitest.ducnv.tikitest.injection.component

import javax.inject.Scope

/**
 * Created by duc on 5/7/2018.
 */

@MustBeDocumented
@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScoped



